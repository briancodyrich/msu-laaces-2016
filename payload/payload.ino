/*****************************************************************
*               LaACES 2016 Payload Code              
*Authors:       Brian Rich, Nathan Webber
*Last Modified: 3/25/2016
******************************************************************/


#include "DHT.h"
#include <SPI.h>
#include <SD.h>
#include <rtc_clock.h>

/****************************************************************
 * hardware Definitions
 ****************************************************************/
// Analog Pin Assignments
#define P_PIN A8                  //  Pin Analog8 Is Pressure Sensor
#define T_PIN A9                  //  Pin Analog9 Is The External Temperature Sensor On Interface Board
#define H_PIN A10                 //  Pin Analog10 Is The hIh-4000 humidity Sensor
#define T_EXT_PIN A0              //  Pin Analog0 Is The Internal Temperature Sensor
#define T_INT_PIN A1              //  Pin Analog1 Is The External Temperature Sensor
#define T_EXT_ON 4                //  Digital Pin 3 Turns On External Temp Sensor
#define T_INT_ON 3                //  Digital Pin 4 Turns On Internal Temp Sensor

// humidity Sensor Stuff
#define DHTPIN 2                    // DhT22 Is Connected To Pin 2
#define DHTTYPE DHT22               // humidity Sensor Is A DHT22

// Temp delay
#define TEMP_SENSOR_WARMUP_TIME 50       // Warmup time for internal temperature sensor in ms

// SD Card
#define CHIP_SELECT 4 //pin four is the SD card

/****************************************************************
 * Software Definitions
 ****************************************************************/
#define TRANSMIT_ERRORS true //transmit errors over the XBee
#define DATA_LOG_PATH "datalog.csv"
#define EX_TEMP1_PATH "ex_temp1"
#define EX_TEMP2_PATH "ex_temp2"
#define INT_TEMP_PATH "int_temp"
#define PRESS_PATH "pressure"
#define DHT_PATH "dht"
#define HIH_PATH "hih"

#define TRANSMISSION_CYCLE_MIN 3000 //  DhT humidity Sensor has A Lag Of ~2 Seconds, Make sure the time is at least 3 seconds
#define TRANSMISSION_CYCLE_TIME 3000
#define TIME_BETWEEN_TRANSMISSIONS 1000

/****************************************************************
 * Global Variables
 ****************************************************************/
DHT dht(DHTPIN, DHTTYPE);     // humidity sensor
RTC_clock rtc_clock(XTAL);    //  Real time clock



/****************************************************************
 * Function Definitions
 ****************************************************************/
void logData(String filepath, String data);
String getTimestamp(void);
 
void setup() {
  // Initalize Serial Commuinications
  Serial.begin(9600);

  //SD card test
  Serial.print("Initializing SD card...");
  if (!SD.begin(CHIP_SELECT)) {
    Serial.println("Card failed, or not present");    // Print error message
  } else
    Serial.println("card initialized.");
    logData(DATA_LOG_PATH, "tsec,YYYY-MM-DD T hh-mm-ss,t1raw,t1deg,t2raw,t2deg,t_int_raw,t_int_deg,Praw,Pmbar,AM2302_humid,AM2302*C,AM2302*F,HIH-4000_Raw,HIH-4000_Percent,");
  
  //set up pins
  pinMode(T_EXT_PIN, INPUT);
  pinMode(T_INT_PIN, INPUT);
  pinMode(T_EXT_ON, OUTPUT);
  pinMode(T_INT_ON, OUTPUT);
  pinMode(P_PIN, INPUT);
  pinMode(T_PIN, INPUT);
  pinMode(H_PIN, INPUT);
  analogReadResolution(12);         //  Analog Resolution Is 0 - 3.3V = 0 - 4096 Counts
  
  dht.begin(); // start humidity sensor
  
  //  Set The RTC Time here
  rtc_clock.init();
  rtc_clock.set_time(23, 5, 9);    //  HH,MM,SS
  rtc_clock.set_date(19, 3, 2016);  //  DD,MM,YYYY
  
  //  Transistors For Additional Temperature Sensors Are PNP.  high Out = Transistor Off
  digitalWrite(T_EXT_ON,HIGH);
  digitalWrite(T_INT_ON, HIGH);
  
}

void loop() {
  int time_sec = millis()/1000;          // Elapsed Time In Seconds
  
  /****************************************************************
   *                External Temperature 1
   ****************************************************************/
  int ext_temp1_raw = analogRead(T_PIN);                                // Raw Temperature
  float ext_temp1_deg = 24.6 - ((ext_temp1_raw - 544)/27.54716981132);  // Temperature in C
  
  /****************************************************************
   *                External Temperature 2
   ****************************************************************/
  digitalWrite(T_EXT_ON, LOW);                                          // Turn On The External Tempreature Diode
  delay(TEMP_SENSOR_WARMUP_TIME);                                       // Warmup time
  int ext_temp2_raw = analogRead(T_EXT_PIN);                            // Raw Temperature
  digitalWrite(T_EXT_ON, HIGH);                                         // Turn Off External Temperature Diode
  float ext_temp2_deg = 24.6 - ((ext_temp2_raw - 798)/2.1518987341772); // Temperature in C

  /****************************************************************
   *                Internal Temperature
   ****************************************************************/
  digitalWrite(T_INT_ON, LOW);                                      // Turn On Internal Temperature Diode
  delay(TEMP_SENSOR_WARMUP_TIME);                                   // Warmup Time
  int int_temp_raw = analogRead(T_INT_PIN);                         // Raw Temperature
  digitalWrite(T_INT_ON, HIGH);                                     // Turn Off Internal Temperature Diode
  float int_temp_deg = 24.6 - ((int_temp_raw - 693)/2.47340475532); // Temperature in C
  
  /****************************************************************
   *                Pressure
   ****************************************************************/
  int press_raw = analogRead(P_PIN);             // Raw Pressure
  float press_mbar = (press_raw / 3.7236363636); // Absolute Pressure in mbars
  
  /****************************************************************
   *                 DHT
   ****************************************************************/ 
  float dht_humidity = dht.readHumidity();      // Humidity 
  float dht_temp_f = dht.readTemperature();     // Temperature in F
  float dht_temp_c = dht.readTemperature(true); // Temperature in C

  /****************************************************************
   *                 HIH-4000
   ****************************************************************/ 
  int hih_humidity_raw = analogRead(H_PIN);                       // Humidity raw
  float hih_humidity_percent = ((hih_humidity_raw-124) / 38.48);  // Humidity percent

  /****************************************************************
   *                 Data logging
   ****************************************************************/ 
   String time_string = String(time_sec);
   //raw data
   logData(DATA_LOG_PATH, time_string + "," + getTimestamp() + "," + ext_temp1_raw + "," + ext_temp1_deg + "," + ext_temp2_raw + "," +ext_temp2_deg + "," + int_temp_raw + "," + int_temp_deg + "," + press_raw + "," + 
   press_mbar + "," + dht_humidity + "," + dht_temp_c + "," + dht_temp_f + "," + dht_temp_c + "," + hih_humidity_raw + "," + hih_humidity_percent + ",");
   
   //seperated data
   logData(EX_TEMP1_PATH, time_string + "," + ext_temp1_deg);
   logData(EX_TEMP2_PATH, time_string + "," + ext_temp2_deg);
   logData(INT_TEMP_PATH, time_string + "," + int_temp_deg);
   logData(PRESS_PATH, time_string + "," + press_mbar);
   logData(DHT_PATH, time_string + "," + dht_humidity);
   logData(HIH_PATH, time_string + "," + hih_humidity_percent);

  /****************************************************************
   *                 Transmission Cycle
   ****************************************************************/ 
   String transmitString = time_string + " " + ext_temp1_deg + " " +ext_temp2_deg + " " + int_temp_deg + " " + dht_humidity + " " + dht_temp_c + " " + press_mbar + " " + dht_humidity + " " + hih_humidity_percent;
   int cycle_start = millis();
   while(millis() - cycle_start < max(TRANSMISSION_CYCLE_TIME, TRANSMISSION_CYCLE_MIN))
   {
    Serial.println(transmitString);
    delay(TIME_BETWEEN_TRANSMISSIONS);
   }
}

void logData(String filepath, String data)
{
  File dataFile = SD.open(filepath, FILE_WRITE);
  if (dataFile) {
    dataFile.println(data);
    dataFile.close();
  } else if (TRANSMIT_ERRORS){
    Serial.println("SD WRITE ERROR " + filepath);
  }

}

String getTimestamp(void)
{
  return String(rtc_clock.get_years()) + "-" + rtc_clock.get_months() + "-" + rtc_clock.get_days() + " " + rtc_clock.get_hours() + ":" + rtc_clock.get_minutes() + ":" +rtc_clock.get_seconds();
}

